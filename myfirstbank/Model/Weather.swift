//
//  Weather.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

struct Weather: Codable {
    
    let weather: [WeatherElement]
}
