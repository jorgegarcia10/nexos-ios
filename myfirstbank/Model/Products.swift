//
//  Products.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

class Products {
    
    var number: String
    var balance: Double
    var type: String
    
    init(number: String, balance: Double, type: String) {
        self.balance = balance
        self.number = number
        self.type = type
    }
}
