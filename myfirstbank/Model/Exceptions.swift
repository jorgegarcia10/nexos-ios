//
//  Exceptions.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation
enum Exceptions: Error{
    case ApplicationException(message: String)
}
