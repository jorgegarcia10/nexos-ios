//
//  WeatherService.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation

class WeatherService {
        
        init(){}
        func excecuteService(completion: @escaping (Weather?, AnyObject?, Bool) -> Void) {
            let serviceUrl: String = "http://api.openweathermap.org/data/2.5/weather?lat=3.43722&lon=-76.522499&appid=63620734a4a8cd0cdc15552257aa0f79"
            let url = URL(string: serviceUrl)
            var urlRequest = URLRequest(url: (url)!)
            urlRequest.httpMethod = "get"
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let config = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 10.0
            let session = URLSession(configuration: config)
            let task = session.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
                if error != nil{
                    completion(nil, error as AnyObject, true)
                    return
                }
                if data == nil{
                    completion(nil, error as AnyObject, true)
                    return
                }
                if let httpResponse = response as? HTTPURLResponse{
                    if httpResponse.statusCode == 200 {
                        do {
                            let decoder = JSONDecoder()
                            let weather = try decoder.decode(Weather.self, from: data!)
                            completion(weather, nil, false)
                            return
                        } catch {
                            completion(nil, error as AnyObject, true)
                            return
                        }
                    } else {
                        completion(nil, error as AnyObject, true)
                    }
                } else {
                    completion(nil, error as AnyObject, true)
                    return
                }
            })
            task.resume()
        }
    }
