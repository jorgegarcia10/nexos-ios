//
//  WeatherRequest.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation
class WeatherRequest {
       
    var response: AnyObject?
    
    init() {}
    func showAccounts() -> AnyObject{
        
        let group = DispatchGroup()
        group.enter()
        WeatherService().excecuteService() {  (data, error, issue) in
            if let data = data {
                self.response = data as AnyObject
            }
            if let error = error {
                self.response = error.localizedDescription! as AnyObject
            }
            group.leave()
        }
        group.wait()
        return self.response!;
        
    }
}
