//
//  MySQLConnection.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import Foundation
import SQLite

class SQLiteConnection{
    
    var database: Connection!
    let products = Table("products")
    let id = Expression<Int>("id")
    let number = Expression<String>("number")
    let balance = Expression<Double>("balance")
    let type = Expression<String>("type")
    
    func connect() {
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
            let fileUrl = documentDirectory.appendingPathComponent("products").appendingPathExtension("sqlite3")
            let database = try Connection(fileUrl.path)
            self.database = database
        
        } catch {
            print(error)
        }
    }
    
    func createTable(){
        
        let createTable = self.products.create(ifNotExists: true) { table in
            table.column(self.id, primaryKey: true)
            table.column(self.number)
            table.column(self.balance)
            table.column(self.type)
        }
        
        do {
            try self.database.run(createTable)
            print("Created table 'Products'")
            
        } catch {
            print(error)
        }
    }
    
    func insertData() throws {

        let insertAccount = self.products.insert(self.number <- "2341-2342-2123-4321", self.balance <- 120983, self.type <- "Savings")
        let insertAccount2 = self.products.insert(self.number <- "7896-7642-2654-6721", self.balance <- 134566, self.type <- "Savings")
        let insertAccount3 = self.products.insert(self.number <- "6567-6578-0987-8781", self.balance <- 1876786, self.type <- "Savings")
        let insertAccount4 = self.products.insert(self.number <- "1241-4562-6543-4321", self.balance <- 1897678, self.type <- "Credit")
        let insertAccount5 = self.products.insert(self.number <- "8781-7892-7654-2345", self.balance <- 323456, self.type <- "Credit")
        let insertAccount6 = self.products.insert(self.number <- "0987-9876-3456-5434", self.balance <- 987654, self.type <- "Credit")
        
        try self.database.run(insertAccount)
        try self.database.run(insertAccount2)
        try self.database.run(insertAccount3)
        try self.database.run(insertAccount4)
        try self.database.run(insertAccount5)
        try self.database.run(insertAccount6)
    }
    
    func listProducts() throws -> [Products]{
        do {
            var userProducts: [Products] = []
            let accounts = try self.database.prepare(self.products)
            for account in accounts {
                //Return object
                let product = Products(number: account[self.number], balance: account[self.balance], type: account[self.type])
                userProducts.append(product)
            }
            return userProducts
        } catch {
            throw Exceptions.ApplicationException(message: "Pruebas")
        }
        
    }
        
}

