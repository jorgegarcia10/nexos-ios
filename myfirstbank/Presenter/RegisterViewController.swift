//
//  RegisterViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

   
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func registerTapped(_ sender: UIButton) {
        let userEmail = emailTxt.text
        let userPassword = passwordTxt.text
        let userConfirm = confirmTxt.text
        
        //Validations
        if(userEmail!.isEmpty || userPassword!.isEmpty || userConfirm!.isEmpty){
            message(message: "Enter data please")
            return;
        }
        
        if(userPassword != userConfirm){
            message(message: "The passwords must be equal.")
            return;
        }
        
        //Save user info
        
        UserDefaults.standard.set(userEmail, forKey: "userEmail")
        UserDefaults.standard.set(userPassword, forKey: "userPassword")
        UserDefaults.standard.synchronize()
        
        //Display info message
        
        let alert = UIAlertController(title: "Alert", message: "Registro exitoso", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){action in
            self.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func message(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func accountTapped(_ sender: UIButton) {
    }

}
