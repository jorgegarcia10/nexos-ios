//
//  WebViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

   
    @IBOutlet weak var webView: WKWebView!
    
    var url = URL(string: "")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlRequest = URLRequest(url: url!)
        webView.load(urlRequest)
        
    }
    


}
