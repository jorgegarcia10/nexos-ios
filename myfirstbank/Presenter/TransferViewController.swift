//
//  TransferViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class TransferViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
     
    @IBOutlet weak var dataTxt: UITextField!
    @IBOutlet weak var codeImg: UIImageView!
    @IBOutlet weak var accountsPicker: UIPickerView!
    
    var products = [Products]()
    var data = [String : NSData]()
    var account = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let connection = SQLiteConnection()
        connection.connect()
        products = try! connection.listProducts()
        
    }
    
    @IBAction func generateTapped(_ sender: UIButton) {
        if let text = dataTxt.text {
            let data = text.data(using: .ascii, allowLossyConversion: false)
            let account = self.account.data(using: .ascii, allowLossyConversion: false)
            let reference = "QRRef1".data(using: .ascii, allowLossyConversion: false)
            self.data.updateValue(data! as NSData, forKey: "value")
            self.data.updateValue(account! as NSData, forKey: "account")
            self.data.updateValue(reference! as NSData, forKey: "reference")
            
            let archived = try! NSKeyedArchiver.archivedData(withRootObject: self.data, requiringSecureCoding: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")
            filter?.setValue(archived, forKey: "inputMessage")
            
            let ciImage = filter?.outputImage
            
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            let transformImage = ciImage?.transformed(by: transform)
            
            let image = UIImage(ciImage: transformImage!)
            codeImg.image = image
        }
        
    }
    
    @IBAction func transferTapped(_ sender: UIButton) {
        screenshoot()
    }
    
    func screenshoot(){
        
        let layer = UIApplication.shared.keyWindow!.layer
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale)
        
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        UIImageWriteToSavedPhotosAlbum(screenshot!, nil, nil, nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
           return 1
        }
       
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.products.count
   }
     
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return products[row].number
   }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.account = products[row].number
    }
}
