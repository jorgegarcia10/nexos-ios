//
//  ViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var products: [Products] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 90.0
        let connection = SQLiteConnection()
        connection.connect()
        connection.createTable()
        try! connection.insertData()
        products = try! connection.listProducts()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let logged = UserDefaults.standard.bool(forKey: "logged")
        if(!logged){
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
        
    }

    @IBAction func logoutTapped(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "logged")
        UserDefaults.standard.synchronize()
        
        self.performSegue(withIdentifier: "loginView", sender: self)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AccountsTableViewCell
        cell.numberLbl.text = products[indexPath.row].number
        cell.balanceLbl.text = String(format: "%.1f", products[indexPath.row].balance)
        cell.typeLbl.text = products[indexPath.row].type
        
        return cell
    }
    

    
}

