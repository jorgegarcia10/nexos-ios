//
//  WeatherViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/6/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    @IBOutlet weak var weatherLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    var weather: AnyObject?
    override func viewDidLoad() {
        super.viewDidLoad()
        getResponse()
        
        // Do any additional setup after loading the view.
    }
    
    func getResponse() {
        self.weather =  WeatherRequest().showAccounts()
        if let parse = self.weather as? Weather {
            for weatherElement in parse.weather {
                self.weatherLbl.text = weatherElement.main
                self.descriptionLbl.text = weatherElement.weatherDescription
            }
        }
       
    }
    

    /*
    // MARK: - Navigation
3687925
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
