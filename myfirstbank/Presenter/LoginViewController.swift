//
//  LoginViewController.swift
//  myfirstbank
//
//  Created by Luis Enrique Cano Tamayo on 10/5/19.
//  Copyright © 2019 Jorge García. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func loginTapped(_ sender: UIButton) {
        let userEmail = emailTxt.text
        let userPassword = passwordTxt.text
        
        //Valitations
        if(userEmail!.isEmpty || userPassword!.isEmpty){
            message(message: "Complete form please.")
        }
        
        
        let userEmailStored = UserDefaults.standard.string(forKey: "userEmail");
        let userPasswordStored = UserDefaults.standard.string(forKey: "userPassword");
        
        if(userEmail == userEmailStored && userPassword == userPasswordStored){
            //Login successfull
            UserDefaults.standard.set(true, forKey: "logged")
            UserDefaults.standard.synchronize()
            self.dismiss(animated: true, completion: nil)
        } else {
            message(message: "Please enter correct data.")
        }
    }
    
    func message(message: String){
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
        
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func accountTapped(_ sender: UIButton) {
    }
}
